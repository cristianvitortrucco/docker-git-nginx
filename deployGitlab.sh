#!/bin/bash
echo "Entrando no script de deploy para o serviço $DOCKER_SERVICE..."
mkdir -p "$CI_PROJECT_DIR/.dockercert"
echo "$DOCKER_CA" > $CI_PROJECT_DIR/.dockercert/ca.pem 
echo "$DOCKER_CERT" > $CI_PROJECT_DIR/.dockercert/cert.pem 
echo "$DOCKER_KEY" > $CI_PROJECT_DIR/.dockercert/key.pem 
export DOCKER_CERT_PATH="$CI_PROJECT_DIR/.dockercert" \
       DOCKER_TLS_VERIFY=1 \
              DOCKER_HOST="$DOCKER_URL"
              echo "DOCKER_CERT_PATH=$CI_PROJECT_DIR/.dockercert"
              echo "DOCKER_TLS_VERIFY=1"
              echo "DOCKER_HOST=$DOCKER_URL"
              docker -v
              docker version
              docker node ls
              if docker service ls --filter "name=$DOCKER_SERVICE" | grep -q "$DOCKER_SERVICE"; then
                  echo "Servico $DOCKER_SERVICE já instalado, atualizando..."
                      docker service update --with-registry-auth --image $IMAGE_TAG --env-add UPDATE="$CI_BUILD_ID" "$DOCKER_SERVICE"
                      else
                          echo "Servico $DOCKER_SERVICE será instalado..."
                              docker service create --with-registry-auth \
                                      --name "$DOCKER_SERVICE" --publish 80:80 --replicas 2 \
                                              $IMAGE_TAG
                                              fi

